﻿namespace BattleShip
{
    public enum HitType
    {
        Water,
        Ship,
        Obstacle
    }
}