﻿namespace BattleShip
{
    public interface IController
    {
        void SetHorizontal(float value);
        void SetVertical(float value);
    }
}