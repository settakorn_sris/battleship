﻿using BattleShip.Manager;
using BattleShip.UI.Game;
using UnityEngine;

namespace BattleShip.Listener
{
    public class HealthBarListener : BaseListener
    {
        [SerializeField] private HealthBarUI healthBarUI;

        protected override void Initialize()
        {
            GameManager.Instance.OnNextTurn += OnNextTurnEvent;
            GameManager.Instance.OnGameOver += OnGameOverEvent;
        }

        protected override void Uninitialized()
        {
            GameManager.Instance.OnNextTurn -= OnNextTurnEvent;
            GameManager.Instance.OnGameOver -= OnGameOverEvent;
        }

        private void OnNextTurnEvent(Team team)
        {
            var clampHealth = team.ActivePlayer.Health / team.ActivePlayer.MaxHealth;
            healthBarUI.SetFillAmount(clampHealth);
            healthBarUI.Show();
        }

        private void OnGameOverEvent(Team winner)
        {
            healthBarUI.Hide();
        }
    }
}