﻿using BattleShip.Manager;

namespace BattleShip.State
{
    public abstract class GameState
    {
        private GameState _gameState;
        private State _currentState;
        protected readonly GameManager GameManager;

        protected GameState(GameManager gameManager)
        {
            GameManager = gameManager;
            _gameState = this;
            _currentState = State.Enter;
        }

        public void SetNextState(GameState gameState)
        {
            _gameState = gameState;
            Exit();
        }

        private enum State
        {
            Enter,
            Update,
            Exit
        }

        protected virtual void Enter()
        {
            _currentState = State.Update;
        }

        protected virtual void Update()
        {
            _currentState = State.Update;
        }

        protected virtual void Exit()
        {
            _currentState = State.Exit;
        }

        public GameState Process()
        {
            switch (_currentState)
            {
                case State.Enter:
                    Enter();
                    break;
                case State.Update:
                    Update();
                    break;
                case State.Exit:
                    break;
                default:
                    Exit();
                    break;
            }

            return _gameState;
        }
    }
}