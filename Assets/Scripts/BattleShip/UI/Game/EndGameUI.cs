﻿using BattleShip.Manager;
using BattleShip.System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BattleShip.UI.Game
{
    public class EndGameUI : BaseUI
    {
        [SerializeField] private TextMeshProUGUI winnerText;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button mainMenuButton;

        private void Awake()
        {
            restartButton.onClick.AddListener(() =>
            {
                GameManager.Instance.Restart();
                GameManager.Instance.OnButtonClickEvent();
            });
            mainMenuButton.onClick.AddListener(() =>
            {
                GameManager.Instance.MainMenu();
                GameManager.Instance.OnButtonClickEvent();
            });
        }

        public void SetWinner(Team winner)
        {
            winnerText.SetText($"{winner.TeamName} WIN THE GAME!");
        }
    }
}