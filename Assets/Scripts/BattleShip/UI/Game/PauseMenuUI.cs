﻿using BattleShip.Manager;
using UnityEngine;
using UnityEngine.UI;

namespace BattleShip.UI.Game
{
    public class PauseMenuUI : BaseUI
    {
        [SerializeField] private Button continueButton;
        [SerializeField] private Button leaveButton;

        private void Awake()
        {
            Debug.Assert(continueButton != null, "continueButton can't be null!");
            Debug.Assert(leaveButton != null, "leaveButton can't be null!");

            continueButton.onClick.AddListener(() =>
            {
                Hide();
                GameManager.Instance.OnButtonClickEvent();
            });

            leaveButton.onClick.AddListener(() =>
            {
                GameManager.Instance.MainMenu();
                GameManager.Instance.OnButtonClickEvent();
            });
        }

        public void Toggle()
        {
            if (gameObject.activeSelf) Hide();
            else  Show();
        }
    }
}