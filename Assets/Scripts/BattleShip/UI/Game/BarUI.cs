﻿using UnityEngine;
using UnityEngine.UI;

namespace BattleShip.UI.Game
{
    public class BarUI : BaseUI
    {
        [SerializeField] protected Image barImage;

        public virtual void Awake()
        {
            Debug.Assert(barImage != null, "barImage can't be null!");
        }

        public virtual void SetFillAmount(float value)
        {
            barImage.fillAmount = value;
        }
    }
}